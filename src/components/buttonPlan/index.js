import React from 'react';
import styles from './index.module.scss';

function ButtonPlan({onClick}) {
    return (
        <div>
            <button onClick={onClick}  className={styles.buttonPlan}>
               Buscar
            </button>
        </div>
    )
}

ButtonPlan.propTypes = {

}

export default ButtonPlan
