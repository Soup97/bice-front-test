import React, { useState } from 'react';
import styles from './index.module.scss';
import axios from 'axios'

import CardPlan from '../cardPlan/index'
import ButtonPlan from '../buttonPlan/index';

const SelectorPlan = ({
    selectorID
}) => {
    const [selectID, setSelectID] = useState('')
    const [seguroPlan, setSeguroPlan] = useState(null)


    const handleChange = (event) => {
        setSelectID(event.target.value);

    }

    const onSearch = (planID) => {
        axios.get('https://dn8mlk7hdujby.cloudfront.net/interview/insurance/' + planID)
            .then((res) => {
                setSeguroPlan(res.data.insurance)
            })
    }


    return (
        <div className={styles.mainContainer}>
            <div>
                <div className={styles.labelSlct}>
                    <label>Escoja su seguro:</label>
                </div>
                <div>
                    <div>
                        <select onChange={(e) => handleChange(e)} className={styles.selectPlan}>
                            <option>Seleccione Seguro</option>
                            <option value="58">Seguro Vida Activa</option>
                            <option value="59">Seguro Vida Protegido</option>
                        </select>

                    </div>
                    <div className={styles.btnPlan}>
                        <ButtonPlan onClick={() => onSearch(selectID)} />
                    </div>

                </div>

            </div>

            {seguroPlan &&
                <div>
                    <CardPlan
                        label={seguroPlan.price}
                        titulo={seguroPlan.name}
                        parrafo={seguroPlan.description}
                        imgUrl={seguroPlan.image}
                    />
                </div>
            }






        </div>
    )
}

SelectorPlan.propTypes = {

}

export default SelectorPlan

