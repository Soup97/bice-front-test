import React, { Fragment, useState, useEffect } from 'react'
import styles from './index.module.scss'

const CardPlan = ({
    imgUrl,
    label,
    titulo,
    parrafo,
}) => {
    return (
        <Fragment>
            <div className={styles.cardPlan}>
                <div className={styles.cardHeader}>
                    <div className={styles.imageCont}>
                        <img alt="sd" className={styles.image} src={imgUrl} />
                    </div>
                    <div>
                        <label className={styles.label}>
                            ${label}
                    </label>
                    </div>
                </div>
                <div className={styles.cardBody}>
                    <div className={styles.titulo}>
                        {titulo}
                    </div>
                    <div className={styles.parrafo}>
                        {parrafo}
                    </div>
                </div>
            </div>

        </Fragment>
    )
}

CardPlan.defaultProps = {
    imgUrl : 'https://i0.wp.com/www.bloguero-ec.com/wp-content/uploads/2014/06/load-jquery.jpg?fit=640%2C460&ssl=1',
    label: 'label',
    titulo: 'Título',
    parrafo: 'Párrafo',
}
    

export default CardPlan;
