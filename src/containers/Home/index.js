import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import CardPlan from '../../components/cardPlan/index';
import SelectorPlan from '../../components/selectorPlan/index';
import ButtonPlan from '../../components/buttonPlan/index'
import axios from 'axios';

import styles from './index.module.scss'

const Home = ({}) => {
    return (
        <div className={styles.main}>
            <SelectorPlan  />
        </div>
    )
}

Home.propTypes = {
    selectorId: PropTypes.string,
}

Home.defaultProps = {
    selectorId: 'wena'
}

export default Home

